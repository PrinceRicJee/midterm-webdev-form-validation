
$(document).ready(function () {

    $error = ""

    $("#register").on("click", function (e) {
        e.preventDefault()

        // Validation firstname
        if ($("#firstname").val() == "") {
            $error += "*First Name is required !"
        } else {
            if ($("#firstname").val().charAt(0) == '.') {
                $error += "<br>*Name must not start with dot !"
            } else {
                if (checkForSpecialChar($("#firstname").val())) {
                    $error += "<br>*Name must not contain special characters !"
                }
            }
        }

        // For lastname
        if ($("#lastname").val() == "") {
            $error += "<br>*Last Name is required !"
        } else {
            if ($("#lastname").val().charAt(0) == '.') {
                $error += "<br>*Name must not start with dot !"
            } else {
                if (checkForSpecialChar($("#lastname").val())) {
                    $error += "<br>*Name must not contain special characters !"
                }
            }
        }


        // For email
        if ($("#email").val() == ""){
            $error += "<br>*Email Address is required !"

        }else{
            validateEmail($("#email").val())
            validSpecialEmail($("#email"))
        }
       
     


       
        // For password
        if ($("#password").val() == "") {
            $error += "<br>*Password is required !"
        } else {
            if ($("#password").val() != $("#confirmPass").val()) {
                $error += "<br>*Password does not match ! Please try again!"
            }

            if ($("#password").val().length < 8) {
                $error += "<br>*Password must atleast 8 characters !"
            } else {
                
                    validatePassword($("#password"))
                
               
            } 
        }
        // For Birthdate
        if ($("#birthdate").val() == "") {
            $error += "<br>*Birthdate is required !";
        } else {
            $today = new Date();
            $dob = new Date($("#birthdate").val());
            $age = Math.floor(($today - $dob) / (365.25 * 24 * 60 * 60 * 1000));
            if ($age < 18) {
                $error += "<br>*Age should be minimum 18!";
            }
        }

        // For Phone Number
        if ($("#phonenumber").val() == "") {
            $error += "<br>*Phone number is required !"
        } else {
            if (!phone_validate($("#phonenumber").val())) {
                $error += "<br>Invalid Phone Number! Your phone number is not on PH origin. Accounts are exclusive for Philippine users only\n"
            }else if($("#phonenumber").val().length >= 13){
                $error += "<br>*Invalid Phone Number! Length should be 12 !";
            }else if($("#phonenumber").val().length <= 13){
                $error += "<br>*Invalid Phone Number! Length should be 12 1";
            }

        }


        if($error == ""){
            $(".modal-title").html("Login Succesfully!")
            $(".modal-header").removeClass("bg-danger")
            $(".modal-header").addClass("bg-success")
            $("#alert").html("<br>Succesfully Register !")
            $("#modal").modal("show")
        }else{
            $(".modal-title").html("Something Went Wrong!")
            $(".modal-header").removeClass("bg-success")
            $(".modal-header").addClass("bg-danger")
            $("#alert").html($error)
            $("#modal").modal("show")

            $error = ""
    
        }
    })

    // Validation for special characters(names)
    var specialChar = "<>@!#$%^&*()_+[]{}?:;|'\"\\,/~`-=";
    function checkForSpecialChar(string) {
        for (i = 0; i < specialChar.length; i++) {
            if (string.indexOf(specialChar[i]) > -1) {
                return true
            }
        }
        return false;
    }

    
    //Validation for special characters(email)

    function validSpecialEmail($email) {
        if ($email.val().charAt(0) == '.' || $email.val().includes("..") || $email.val() == "") {
            $error += "<br>*Email Address is invalid! Please try again !"
        }
    }

    //Validation for email
    function validateEmail(email) {
        var regexPattern = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
        if (regexPattern.test(email)) {
            if (email.indexOf('com', email.length - 3) !== -1 || email.indexOf('net', email.length - 3) !== -1 || email.indexOf('org', email.length - 3) !== -1 || email.indexOf('ph', email.length - 2) !== -1) {
            } else {
                $error+='<br>*Email domain must be .net, .org, .com, .ph';
            }
        } else {
            $error+='<br>*Not a valid e-mail address !';
        }
     }

    // Validation for Password 
    function validatePassword(pass) {
        var upperCase = new RegExp('[A-Z]');
        var lowerCase = new RegExp('[a-z]');
        var numbers = new RegExp('[0-9]');
        if ($(pass).val().match(upperCase) && $(pass).val().match(lowerCase) && $(pass).val().match(numbers)) {
        }
        else {
                $error+="<br>*Password must contain atleast one upper and lower case letters, and at least one number or symbol.";
       
            }
    }

    // Validation for Phone Number
    function phone_validate(phoneNum) {
        var regexPattern = new RegExp(/^[0-9-+]+$/);
        return regexPattern.test(phoneNum);
    }


    //Show Password 
    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');
    togglePassword.addEventListener('click', function (e) {
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        this.classList.toggle('fa-eye-slash');
    });

    const togglePassword2 = document.querySelector('#togglePassword2');
    const password2 = document.querySelector('#confirmPass');
    togglePassword2.addEventListener('click', function (e) {
        const type = password2.getAttribute('type') === 'password' ? 'text' : 'password';
        password2.setAttribute('type', type);
        this.classList.toggle('fa-eye-slash');
    });

});